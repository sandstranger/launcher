package com.test.launcher.ui.viewholder;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import butterknife.ButterKnife;

public abstract class AbstractAdapterViewHolder<T> extends RecyclerView.ViewHolder {
    protected final Context context;

    public AbstractAdapterViewHolder(View itemView) {
        super(itemView);
        context = itemView.getContext();
        ButterKnife.bind(this, itemView);
    }

    public abstract void bindView(T dataModel);
}
