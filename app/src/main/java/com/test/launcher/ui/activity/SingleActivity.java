package com.test.launcher.ui.activity;

import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;

import com.test.launcher.R;

import lombok.val;

abstract class SingleActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayoutResId());
        setFragment();
    }

    @LayoutRes
    protected int getLayoutResId() {
        return R.layout.single_activity;
    }

    protected abstract Fragment createFragment();

    private void setFragment() {
        val transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.fragment_container, createFragment());
        transaction.commit();
    }
}
