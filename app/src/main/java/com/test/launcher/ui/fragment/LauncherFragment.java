package com.test.launcher.ui.fragment;

import android.content.pm.ResolveInfo;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.test.launcher.R;
import com.test.launcher.ui.adapter.SingleAdapter;
import com.test.launcher.ui.viewholder.LauncherAdapterViewHolder;
import com.test.launcher.ui.viewholder.LauncherFragmentViewHolder;
import com.test.launcher.utils.ResolveInfoUtils;

import lombok.val;

public class LauncherFragment extends SingleFragment {

    @Override
    protected int getLayoutResId() {
        return R.layout.launcher_fragment;
    }

    @Override
    protected void bindView(View rootView) {
        val viewHolder = new LauncherFragmentViewHolder(rootView);
        viewHolder.setRecyclerViewAdapter(buildAdapterForRecylerView());
    }

    public static Fragment newInstance() {
        return new LauncherFragment();
    }

    private RecyclerView.Adapter buildAdapterForRecylerView() {
        val adapter = new SingleAdapter<ResolveInfo>
                (R.layout.app_item_layout, LauncherAdapterViewHolder.class,
                        ResolveInfoUtils.getAllActivitiesFromSystem(getActivity()));
        return adapter;
    }
}
