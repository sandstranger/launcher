package com.test.launcher.ui.adapter;

import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.test.launcher.ui.viewholder.AbstractAdapterViewHolder;

import java.util.ArrayList;
import java.util.List;

public class SingleAdapter<T> extends RecyclerView.Adapter<AbstractAdapterViewHolder> {
    @LayoutRes
    private final int layoutResourceId;
    private final Class<? extends AbstractAdapterViewHolder> viewHolderClass;
    private final List<T> items;

    public SingleAdapter(int layoutResourceId, Class<? extends AbstractAdapterViewHolder> viewHolderClass, List<T> items) {
        this.layoutResourceId = layoutResourceId;
        this.viewHolderClass = viewHolderClass;
        this.items = new ArrayList<>(items);
    }

    @NonNull
    @Override
    public AbstractAdapterViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(layoutResourceId, parent, false);
        AbstractAdapterViewHolder viewHolder = null;
        try {
            viewHolder = viewHolderClass.getDeclaredConstructor(View.class).newInstance(view);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull AbstractAdapterViewHolder holder, int position) {
        holder.bindView(items.get(position));
    }

    @Override
    public int getItemCount() {
        return items != null ? items.size() : 0;
    }
}
