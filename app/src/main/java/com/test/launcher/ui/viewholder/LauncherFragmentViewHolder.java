package com.test.launcher.ui.viewholder;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.test.launcher.R;
import com.test.launcher.utils.extensions.RecyclerViewExtensions;

import butterknife.BindView;
import butterknife.ButterKnife;
import lombok.experimental.ExtensionMethod;

@ExtensionMethod(RecyclerViewExtensions.class)
public class LauncherFragmentViewHolder {
    @BindView(R.id.apps_recycler_view)
    protected RecyclerView recyclerView;
    private final Context context;

    public LauncherFragmentViewHolder(View rootView) {
        ButterKnife.bind(this, rootView);
        context = rootView.getContext();
        initRecyclerView();
    }

    private void initRecyclerView() {
        recyclerView.setLayoutManager(new LinearLayoutManager(context));
        recyclerView.addDivider();
    }

    public void setRecyclerViewAdapter(RecyclerView.Adapter adapter) {
        recyclerView.setAdapter(adapter);
    }
}
