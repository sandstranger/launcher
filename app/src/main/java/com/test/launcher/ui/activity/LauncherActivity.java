package com.test.launcher.ui.activity;

import android.support.v4.app.Fragment;

import com.test.launcher.ui.fragment.LauncherFragment;

public class LauncherActivity extends SingleActivity {

    @Override
    protected Fragment createFragment() {
        return LauncherFragment.newInstance();
    }
}
