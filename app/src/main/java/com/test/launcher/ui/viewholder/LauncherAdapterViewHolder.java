package com.test.launcher.ui.viewholder;

import android.content.pm.ResolveInfo;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.test.launcher.R;
import com.test.launcher.utils.ResolveInfoUtils;

import butterknife.BindView;
import butterknife.OnClick;

public class LauncherAdapterViewHolder extends AbstractAdapterViewHolder<ResolveInfo> {
    @BindView(R.id.app_name_text_view)
    protected TextView appNameTextView;
    @BindView(R.id.app_image_view)
    protected ImageView appImageView;
    private final ResolveInfoUtils resolveInfoUtils;

    public LauncherAdapterViewHolder(View itemView) {
        super(itemView);
        resolveInfoUtils = new ResolveInfoUtils(context);
    }

    @Override
    public void bindView(ResolveInfo dataModel) {
        resolveInfoUtils.setResolveInfo(dataModel);
        appNameTextView.setText(resolveInfoUtils.getAppName());
        appImageView.setImageDrawable(resolveInfoUtils.getAppIconDrawable());
    }

    @OnClick (R.id.app_clicked_button)
    protected void onAppClickedButton() {
        resolveInfoUtils.startApp();
    }
}
