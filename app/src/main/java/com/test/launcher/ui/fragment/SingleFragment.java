package com.test.launcher.ui.fragment;

import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import lombok.val;

abstract class SingleFragment extends Fragment {

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        val v = inflater.inflate( getLayoutResId(), container, false);
        bindView(v);
        return v;
    }

    @LayoutRes
    protected abstract int getLayoutResId();
    protected abstract void bindView(View rootView);
}
