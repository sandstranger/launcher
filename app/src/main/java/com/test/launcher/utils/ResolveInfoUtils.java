package com.test.launcher.utils;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.drawable.Drawable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import lombok.Setter;
import lombok.val;

public class ResolveInfoUtils {
    @Setter
    private ResolveInfo resolveInfo;
    private final PackageManager packageManager;
    private final Context context;

    public ResolveInfoUtils(Context context) {
        this.context = context;
        packageManager = context.getPackageManager();
    }

    public String getAppName() {
        return resolveInfo != null ? resolveInfo.loadLabel(packageManager).toString() : "";
    }

    public Drawable getAppIconDrawable() {
        return resolveInfo != null ? resolveInfo.loadIcon(packageManager) : null;
    }

    public void startApp() {
        if (resolveInfo == null) {
            return;
        }

        val activityInfo = resolveInfo.activityInfo;
        val intent = new Intent(Intent.ACTION_MAIN);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.setClassName(activityInfo.applicationInfo.packageName, activityInfo.name);
        context.startActivity(intent);
    }

    public static List<ResolveInfo> getAllActivitiesFromSystem(Context context) {
        val pm = context.getPackageManager();

        val iFlags = 0;
        val i = new Intent(Intent.ACTION_MAIN);
        i.addCategory(Intent.CATEGORY_LAUNCHER);

        val activities = pm.queryIntentActivities(i, iFlags);

        if (activities != null) {
            Collections.sort(activities,(a, b) -> String.CASE_INSENSITIVE_ORDER.compare(a.loadLabel(pm).toString(), b.loadLabel(pm).toString()));
            return activities;
        }

        return new ArrayList<>();
    }
}
